ocaml-extunix (0.2.0-1) unstable; urgency=medium

  * Team upload
  * Remove Hendrik from Uploaders
  * New upstream release
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Thu, 06 Aug 2020 10:00:49 +0200

ocaml-extunix (0.1.6-1) unstable; urgency=medium

  * New upstream release
  * Update Vcs-* fields
  * Fix docdir option passed to configure script

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 11 May 2018 13:53:22 +0200

ocaml-extunix (0.1.5-2) unstable; urgency=medium

  * Team upload
  * Fix installation on bytecode architectures (Closes: #868721)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 20 Jul 2017 15:44:09 +0200

ocaml-extunix (0.1.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Switch upstream homepage to github
  * Bump debhelper compat to 10
  * Bump Standards-Version to 4.0.0
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Mon, 17 Jul 2017 18:35:08 +0200

ocaml-extunix (0.1.3-2) unstable; urgency=medium

  * Team upload
  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sun, 16 Jul 2017 13:22:35 +0200

ocaml-extunix (0.1.3-1) unstable; urgency=medium

  * New upstream release
    - Refresh patches.
  * Install .cmt and .annot files in -dev package

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 18 Jan 2016 00:37:21 +0100

ocaml-extunix (0.1.1-1) unstable; urgency=medium

  * New upstream release.
    - Refresh patches.

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 19 Jul 2015 23:46:54 +0000

ocaml-extunix (0.1.0-1) unstable; urgency=medium

  * New upstream release.
    - Update 0001-Add-possibility-to-disable-execinfo-test-through-env.patch
    - Remove fix-text-32-bit.patch (fixed upstream).
  * Add myself to Uploaders.

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 08 May 2014 20:36:54 +0200

ocaml-extunix (0.0.6-1) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Hendrik Tews ]
  * new upstream version
  * bump debhelper compat level and standards version
  * add myself as uploader
  * update Vcs fields, dependencies and package description
  * update copyright
  * add patch fix-test-32-bit to compile and run tests on 32 bit OCaml
  * install api documentation in /usr/share/doc/libextunix-ocaml-dev/api/html
    and use dh_ocamldoc
  * update description of execinfo patch

 -- Hendrik Tews <hendrik@askra.de>  Thu, 13 Jun 2013 15:42:38 +0200

ocaml-extunix (0.0.5-2) unstable; urgency=low

  * Team upload.
  * Revert the following changes:
    - Use --disable-execinfo to disable execinfo test on arm* and powerpc.
    - remove 0001-Add-possibility-to-disable-execinfo-test-through-env.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 27 Jun 2012 14:49:24 +0200

ocaml-extunix (0.0.5-1) unstable; urgency=low

  * Team upload.
  * New upstream release
    - remove 0002-Define-O_CLOEXEC-when-missing.patch, integrated upstream.
    - remove 0001-Add-possibility-to-disable-execinfo-test-through-env.patch.
      Upstream now provides a way to disable some tests.
  * Enable tests explicitly during confidure phase using --enable-tests.
  * Use --disable-execinfo to disable execinfo test on arm* and powerpc.
  * Fix lintian's duplicate-short-description.

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 27 Jun 2012 12:54:54 +0200

ocaml-extunix (0.0.4-2) unstable; urgency=low

  * Team upload.
  * Fix FTBFS on kfreebsd-*
    - add 0002-Define-O_CLOEXEC-when-missing.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 06 Jun 2012 14:00:02 +0200

ocaml-extunix (0.0.4-1) unstable; urgency=low

  * Team upload.
  * New upstream release (Closes: #675505)
    - Rebase patches
  * Install extunix.cmxs, when available
  * Bump Standards-Version to 3.9.3, no changes needed.

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 05 Jun 2012 17:37:13 +0200

ocaml-extunix (0.0.3-2) unstable; urgency=low

  * Team upload
  * Disable extunix test on arm* and powerpc

 -- Stéphane Glondu <glondu@debian.org>  Thu, 25 Aug 2011 09:49:35 +0200

ocaml-extunix (0.0.3-1) unstable; urgency=low

  * Team upload
  * New upstream release (Closes: #632500)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 12 Jul 2011 20:27:12 +0200

ocaml-extunix (0.0.2-1) unstable; urgency=low

  * Team upload

  [ Stéphane Glondu ]
  * New upstream release
  * Add debian/watch
  * Bump Standards-Version to 3.9.2

  [ Sylvain Le Gall ]
  * Fix public-domain license for src/signalfd.c

 -- Stéphane Glondu <glondu@debian.org>  Sat, 02 Jul 2011 20:53:22 +0200

ocaml-extunix (0.0.1-1) unstable; urgency=low

  * Initial release. (Closes: #604065)

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 20 Nov 2010 00:47:32 +0100
